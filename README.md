# C O U N T E R | challenge

Challenge: Understanding the basic ideas of a language/toolkit.
[DOWNLOAD APK](https://drive.google.com/file/d/1ihYDMWoWPYkYDADz0oWP6Q8E3Foq9hV6/view?usp=sharing)

### SCREENSHOTS
-----------

![init-view](screenshots/counter_i.jpeg "Main-view application")
![counter](screenshots/counter_ii.jpeg "+1 when select arrow_up")
![discounter](screenshots/counter_iii.jpeg "-1 when select arrow_down")

### LANGUAGES, LIBRARIES AND TOOLS USED

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [ViewBinding](https://developer.android.com/topic/libraries/view-binding?hl=es-419)

### REQUIREMENTS

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Kotlin Version](https://kotlinlang.org/docs/releases.html)
* [MIN SDK 23](https://developer.android.com/preview/api-overview.html))
* [Build Tools Version 30.0.3](https://developer.android.com/studio/releases/build-tools)
* Latest Android SDK Tools and build tools.

### ARCHITECTURE

- N/A Just activity and view.

## L I C E N S E

Under the [MIT license](https://opensource.org/licenses/MIT).

