package com.hugo.challenge.counter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.hugo.challenge.counter.databinding.ActivityCounterBinding

class CounterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCounterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCounterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val versionName = "v${BuildConfig.VERSION_NAME}"
        binding.textViewVersion.text = versionName

        loadFeature()
    }
    
    private fun loadFeature() {
        var counter = 0
        binding.imageViewArrowUp.setOnClickListener {
            counter += 1
            binding.textViewCounterResult.apply {
                text = counter.toString()
                if (counter >= 0) {
                    setTextColor(
                        ContextCompat.getColor(this@CounterActivity, R.color.colorAccent)
                    )
                }
            }
        }
        binding.imageViewArrowDown.setOnClickListener {
            counter -= 1
            binding.textViewCounterResult.apply {
                text = counter.toString()
                if (counter < 0) {
                    setTextColor(
                        ContextCompat.getColor(this@CounterActivity, R.color.colorAlizarin)
                    )
                }
            }
        }
    }
}